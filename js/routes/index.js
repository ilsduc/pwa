import page from "page";
import * as _ from '../helpers';

const initializeRoutes = routes => {
    // map trought routes
    routes.map(route => {
        // for each one
        const { path, pageName, title, loadData } = route;
        //
        page(path, async ctx => {        
            // Load the page
            const module = await import(`/js/pages/${pageName}.js`);
            const Page = module.default;
            //
            const section = _.slugify(pageName);
            //
            const CurrentPage = _.getPage(section);
            // Reblank page
            CurrentPage.innerHTML = '';

            // default title
            if (typeof title === 'string') _.setTitle(title);
            // 
            let loadedData = null;

            if (!!loadData) {
                const params = ctx.params;

                const loadDataResult = await loadData( params );
                //
                const loadDataResultClone = loadDataResult.clone();
                //
                loadedData = await loadDataResult.json();
                //
                caches.open('runtime-caching').then(cache => {
                    cache.put(loadDataResultClone.url, loadDataResultClone);
                });
                //
                if (typeof title === 'function') {
                    // Set the title
                    _.setTitle(title( loadedData ));
                }
            }
            // Calle the page
            Page(CurrentPage, loadedData);
            //
            _.getPages(routes).forEach(page => page.removeAttribute('active'));
            //
            _.removeFMP();
            //
            CurrentPage.setAttribute('active', true);
        });
    });

    page();
};

export default initializeRoutes;