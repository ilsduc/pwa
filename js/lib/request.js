const DEFAULT_HEADERS = {
    'Accept': 'application/json',
    'Content-type': 'application/json',
}

export const request = {
    get: (url) => fetch(url, {
        method: 'GET',
        headers: DEFAULT_HEADERS,
    }),
    post:(url, item) => fetch(url, {
        method: 'POST',
        body: JSON.stringify(item),
        headers: DEFAULT_HEADERS,
    }),
    put: (url, item) => fetch(url, {
        method: 'PUT',
        body: JSON.stringify(item),
        headers: DEFAULT_HEADERS,
    }),
    patch: (url, item) => fetch(url, {
        method: 'PATCH',
        body: JSON.stringify(item),
        headers: DEFAULT_HEADERS,
    }),
    delete: (url) => fetch(url, {
        method: 'DELETE',
        headers: DEFAULT_HEADERS,
    }),
}