'use strict';
import checkConnectivity from './network.js';
import * as _ from './helpers.js';
import initializeRoutes from './routes';
import { routes } from './routes/routes.js';

(async () => {

  window.addEventListener('beforeinstallprompt', e => {
    console.log('Application is installable');
    e.preventDefault();
    window.installPrompt = e;
  });

  checkConnectivity({
    interval: 2000
  });

  document.addEventListener('connection-changed', e => {
    let root = document.documentElement;
    if (e.detail) {
      root.style.setProperty('--app-bg-primary', '#007eef');
      console.log('Back online');
    } else {
      root.style.setProperty('--app-bg-primary', '#7D7D7D');
      console.log('Connection too slow');
    }
  });

  // Initialize routes
  initializeRoutes(routes);

})();