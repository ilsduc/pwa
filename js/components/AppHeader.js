const AppHeader = props => {

    const {
        title = 'TodosApp',
    } = props;
    
    const headerEl = document.createElement('header');
    headerEl.classList.add('app-header');

    const titleEl = document.createElement('h1');
    titleEl.innerHTML = title;

    headerEl.appendChild(titleEl);

    return headerEl;
};

export default AppHeader;