const Button = function(props) {
    const { 
        title, 
        onClick = () => null,
        inverted,
    } = props;
    
    const el = document.createElement('button');
    el.innerHTML = title;
    el.onclick = onClick ;
    el.classList.add('app-button');

    if (!!inverted) el.classList.add('inverted');

    return el;
}

export default Button;