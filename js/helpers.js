/**
 * Set the document title
 */

 export const setTitle = title => {
    const document_title = document.head.querySelector('title');
    document.title = `${document_title.dataset.base} - ${title}`;
 };

 /**
  * Remove First Meaningful Paint
  */
 export const removeFMP = () => {
  const firstMeaningfulPaint = document.querySelector('section#first-meaningful-paint');
  firstMeaningfulPaint.setAttribute('hidden', '');
 };

 /**
  * 
  * Get a page
  */
 export const getPage = page => app.querySelector(`[page=${page}]`);
 /**
  * 
  * Get all pages
  */
 export const getPages = routes => routes.map(route => getPage(slugify(route.pageName)));


 export const slugify = text => text.toString().toLowerCase()
                                                      .replace(/\s+/g, '-')           // Replace spaces with -
                                                      .replace(/[^\w\-]+/g, '-')       // Remove all non-word chars
                                                      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                                                      .replace(/^-+/, '-')             // Trim - from start of text
                                                      .replace(/-+$/, '-');
 